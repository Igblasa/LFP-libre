<?php
namespace LFPBundle\Services;

class Acciones
{
	public $manager;
	
	public function __construct($manager) {
		$this->manager = $manager;
	}
	
	public function borradoLogico($id)
	{		
		$jug = $this->manager->getRepository('LFPBundle:Club')->find($id);
		$jug->setBorrado(1);
		$flush = $this->manager->flush();
		if($flush==null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}
