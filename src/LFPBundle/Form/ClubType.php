<?php

namespace LFPBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ClubType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
			
			$builder
				->add('nombre',TextType::class, array("label"=>"Nombre:","required"=>"false", "attr" =>array(
					"class" => "form-control",
				)))
				->add('telefono',TextType::class, array("label"=>"Telefono:","required"=>"false", "attr" =>array(
					"class" => "tlf form-control",
				)))
				->add('jugadores', EntityType::class,array(
					"class" => "LFPBundle:Jugadores",
					"label" => "Jugadores:",
					"attr" =>array("class" => "form-control")
				))
				->add('Actualizar', SubmitType::class, array("attr" =>array(
					"class" => "btn btn-success",
				)))
			;
			
			//$builder->add('nombre')->add('telefono')->add('borrado')->add('jugadores');
			

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LFPBundle\Entity\Club'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'lfpbundle_club';
    }


}
